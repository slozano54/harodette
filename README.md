<!-- 
    Source pour l'écriture d'un REAMDE en mardown : https://www.makeareadme.com/ 
-->


<!-- ![Work in progress](http://www.repostatus.org/badges/latest/wip.svg) -->
![Project Status: Inactive – The project has reached a stable, usable state but is no longer being actively developed; support/maintenance will be provided as time allows.](https://www.repostatus.org/badges/latest/inactive.svg)

<!-- _The project has reached a stable, usable state but is no longer being actively developed;_

_Support/maintenance will be provided as time allows._ -->

<style type="text/css">
</style>
<!-- PROJECT LOGO -->
<p text-align="center">
  <a href="https://framagit.org/slozano54/harodette">
    <img src="img/logo.png" alt="Logo" width="80" height="80">
  </a>

  <h1 text-align="center">DIU EIL 2020/2022</h1>
  <h2 text-align="center">BLOC5 - Haro contre le dette - </h2>
  <h3 text-align="center">Auteur - Sébastien LOZANO</h3>

  <p text-align="center">
    <a href="https://framagit.org/slozano54/harodette">Explore the docs</a>
    .    
    <a href="https://framagit.org/slozano54/harodette/issues">Report Bug</a>    
    .
    <a href="https://framagit.org/slozano54/harodette/issues">Request Feature</a>
  </p>
</p>


<!-- TABLE OF CONTENTS -->
<details open="open">
  <summary><h2 style="display: inline-block">Table of Contents</h2></summary>
  <ol>
    <li>
      <a href="#about-the-project">About The Project</a> 
    </li>
    <li>
      <a href="#getting-started">Getting Started</a>
      <ul>
        <li><a href="#prerequisites">Prerequisites</a></li>
        <li><a href="#installation">Installation</a></li>
      </ul>
    </li>
    <li><a href="#roadmap">Roadmap</a></li>
    <li><a href="#contributing">Contributing</a></li>
    <li><a href="#license">License</a></li>
    <li><a href="#contact">Contact</a></li>    
  </ol>
</details>


<!-- ABOUT THE PROJECT -->
## About The Project
### Contexte
Dans une économie moderne, les entreprises sont endettées par rapport à d’autres, qui sont elles-mêmes endettées. Les dettes peuvent même aller dans les deux sens : une entreprise A peut devoir payer une facture à B et cette même entreprise B doit aussi payer une facture à A. Ou de manière plus large, le jeu des dettes peut induire une dette circulaire : A doit de l’argent à B, qui en doit à C qui en doit elle-même à A.

Ces dettes forment une masse financière en attente, et à l’échelle mondiale, cette masse peut atteindre des montants très élevés. Si jamais, il y a une crise financière, c’est alors énormément d’argent qui rapidement doit être mobilisée : toutes les entreprises veulent en même temps qu’on les paie. Ceci peut entraîner un blocage de l’économie.

L’idée est donc de travailler sur toutes les micro-dettes de manière à diminuer le volume global d’argent en dehors des entreprises. Dans le monde financier, on parle de « compensation
mutuelle des dettes »

**Le projet du bloc 5 consiste à implémenter des algorithmes pour la compensation mutuelle des dettes.**

### Les trois algorithmes
#### Une méthode heuristique de réduction de la dette

Cet algorithme qui choisit au hasard une entreprise A, qui donne à A la somme nécessaire
pour rembourser sa plus forte dette, et qui fait « ruisseler » ce processus sur l’entreprise
destinataire du remboursement jusqu’à épuisement du processus.

#### Méthode par élimination des cycles

Si le graphe contient un cycle de dettes de même valeur (A doit x€ à B, qui doit x€ à C, qui doit x€ à A),
alors cette dette peut être annulée dans modifier la positionne nette de chaque entreprise, et ceci sans
intervention extérieure.
Pour appliquer cette idée, nous allons d’abord simplifier le graphe en remplaçant pour chaque
couple d’entreprises A, B l’ensemble des dettes de A vers B par une seule dette égale à la somme
des dettes.

Puis une fois qu’on a trouvé un cycle (A,B,C,D), on a donc un flux de dettes de A vers B, de B vers
A, de C vers D et de D vers A. L’ensemble de ces dettes a un minimum α. Et on peut donc réduire
de α toutes les dettes du cycle. Une des dettes tombe donc à 0, et l’arc correspondant peut être
supprimé, ce qui supprime un cycle.

#### Algorithme de Klein

L’idée de l’algorithme de Klein est d’ajouter des dettes temporaires virtuelles. Au départ, pour
chaque dette de valeur x entre un noeud A et un noeud B, on ajoute une arête inverse de valeur
0 entre le noeud B et le noeud A.

On va modifier pas à pas l’état des dettes de manière à annuler une dette sur un cycle, tout en
maintenant inchangée à chaque étape la position nette de chaque entreprise.

Pour cela, on applique le même principe que la stratégie précédente : on recherche un cycle.
Ce cycle à une dette minimale α. On diminue chaque dette de α, et pour chaque diminution,
afin de garder inchangée la position nette de l’entreprise, on augmente la dette inverse d’une
valeur de α. Attention, ici, le cycle est particulier car il ne doit contenir aucune dette
nulle. En revanche, il peut contenir des inverses non nulles.

Au cours du processus, des dettes s’annulent. Il ne faut pas les supprimer tout de suite, car le
processus peut les pousser à se « désannuler » (l’exemple ci-après montre un tel cas)

Une fois qu’on ne peut plus trouver de cycle, on enlève toutes les dettes annulées, ainsi que
toutes les dettes inverses temporaires.

<!-- GETTING STARTED -->
## Getting Started

To get a local copy up and running follow these simple steps.

### Prerequisites


### Installation

Cloner [le dépot](https://framagit.org/slozano54/harodette)

```shell
git clone https://framagit.org/slozano54/harodette
```

Ou télécharger l'[archive zip](https://framagit.org/slozano54/harodette/-/archive/master/harodette-master.zip).

<!-- ROADMAP -->
## Roadmap

See the [open issues](https://framagit.org/slozano54/harodette/issues) for a list of proposed features (and known issues).

<!-- CONTRIBUTING -->
## Contributing

Contributions are what make the open source community such an amazing place to be learn, inspire, and create. Any contributions you make are **greatly appreciated**.

1. Fork the Project
2. Create your Feature Branch (`git checkout -b feature/AmazingFeature`)
3. Commit your Changes (`git commit -m 'Add some AmazingFeature'`)
4. Push to the Branch (`git push origin feature/AmazingFeature`)
5. Open a Pull Request



<!-- LICENSE -->
## License

[MIT](https://choosealicense.com/licenses/mit/)

<!-- CONTACT -->
## Contact

Sébastien LOZANO - Write me on gitlab

Project Link: [https://framagit.org/slozano54/harodette](https://framagit.org/slozano54/harodette)