#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO

# Source pour l'écriture d'un REAMDE en mardown : https://www.makeareadme.com/
# Génère la page d'accueil de la documentation pdoc3

"""
### DIU EIL 2020/2022 - BLOC5 : HARO CONTRE LA DETTE

![Project Status: Inactive – The project has reached a stable, usable state but is no longer being actively developed; support/maintenance will be provided as time allows.](https://www.repostatus.org/badges/latest/inactive.svg)

<a href="https://framagit.org/slozano54/harodette">
  <img src="https://framagit.org/slozano54/harodette/-/raw/master/img/logo.png" alt="Logo" width="80" height="80">
  <br>
  Dépôt framagit
</a>



L'architecture MVC présente plusieurs avantages dont :

- Faciliter les modifications
- Faciliter la maintenance
- Faciliter les tests
- Autoriser la multiplicité des vues
""" # noqa : E501
pass

# Pour gérer les imports relatifs
import os  # noqa : E402
import sys  # noqa : E402
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
