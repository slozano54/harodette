#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO
"""Gestion des arcs"""

# Pour la gestion des imports relatifs
import sys
import os

SCRIPT_DIR = os.path.dirname(os.path.abspath(__file__))
sys.path.append(os.path.dirname(SCRIPT_DIR))

from models.noeud import Noeud  # noqa: E402


class Arc:
    """La classe Arc sert pour chaque arête(edge) du graphe.

        Attributs
        ---------
        - nom        : str   - Identification d'un arc par un nom par souci de lisibilité de la représentation des objets de la classe Graphe.

        - noeudDebut : Noeud - Une instance de la classe Noeud pour le début de l'arc.

        - noeudFin   : Noeud - Un instance de la classe Noeud pour la fin de l'arc.

        - poids      : int   - Représente le montant de la créance ou le débit entre deux sommets.

        - estInverse : bool  - Un booléen pour savoir si l'arc est un arc inverse

    """  # noqa: E501

    def __init__(
        self,
        nom: str,
        noeudDebut: Noeud,
        noeudFin: Noeud,
        poids=None,
        estInverse=False
    ) -> None:
        """Constructeur

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc1 = Arc('n1n2', n1, n2, 12)
        >>> print(arc1.nom)
        n1n2
        >>> print(arc1.noeudDebut)
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        >>> print(arc1.noeudFin)
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        >>> print(arc1.poids)
        12
        >>> print(arc1.estInverse)
        False
        >>> arc2 = Arc('n1n2Bis', n1, n2, 8, True)
        >>> print(arc2.noeudDebut)
        Noeud(nom=n1, voisins=['n2'], nbArcs=2, capital=0)
        >>> print(arc2.noeudFin)
        Noeud(nom=n2, voisins=['n1'], nbArcs=2, capital=0)
        >>> print(arc2.poids)
        8
        >>> print(arc2.estInverse)
        True
        """
        pass
        assert isinstance(nom, str), "Le nom d'un arc doit être un string."
        assert isinstance(poids, int) or poids is None, "Le poids d'un arc doit être un entier ou None."  # noqa: E501
        assert isinstance(noeudDebut, Noeud), "Le noeud de début de l'arc doit être une instance de Noeud."  # noqa: E501
        assert isinstance(noeudFin, Noeud), "Le noeud de fin de l'arc doit être une instance de Noeud."  # noqa: E501
        assert isinstance(estInverse, bool), "estInverse doit être un booléen"

        self.nom = nom
        self.noeudDebut = noeudDebut
        self.noeudFin = noeudFin
        self.poids = poids
        self.estInverse = estInverse

        # Les deux noeuds deviennent voisins s'ils ne le sont pas déjà
        # sinon on incrémente uniquement le nombre d'arcs de chacun
        if not noeudDebut.isVoisin(noeudFin):
            noeudDebut.addVoisin(noeudFin)
        else:
            noeudDebut.setNbArcs(noeudDebut.getNbArcs() + 1)
            noeudFin.setNbArcs(noeudFin.getNbArcs() + 1)

    def __str__(self) -> None:
        """Représentation en chaine de caractères d'une instance de la classe Arc.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12)
        >>> print(arc)
        Arc(nom=n1n2, noeudDebut=Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0), noeudFin=Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0), poids=12, estInverse=False)
        """  # noqa: E501
        pass
        return "Arc(nom={}, noeudDebut={}, noeudFin={}, poids={}, estInverse={})".format(  # noqa: E501
            self.nom,
            self.noeudDebut,
            self.noeudFin,
            self.poids,
            self.estInverse
        )

# ================================================================
# =========== GETTERS ============================================
# ================================================================
    def getNom(self) -> str:
        """Renvoie le nom de l'Arc.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12)
        >>> arc.getNom()
        'n1n2'
        """
        pass
        return self.nom

    def getPoids(self) -> int:
        """Renvoie le poids de l'Arc.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12)
        >>> arc.getPoids()
        12
        """
        pass
        return self.poids

    def getNoeudDebut(self) -> Noeud:
        """Renvoie le noeud de début de l'Arc.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12)
        >>> print(arc.getNoeudDebut())
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        """
        pass
        return self.noeudDebut

    def getNoeudFin(self) -> Noeud:
        """Renvoie le noeud de fin de l'Arc.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12)
        >>> print(arc.getNoeudFin())
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        """
        pass
        return self.noeudFin

    def getEstInverse(self) -> bool:
        """Renvoie un booléen selon que l'arc est un arc inverse ou non.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12, False)
        >>> print(arc.getEstInverse())
        False
        >>> arc1 = Arc('n1n2Bis', n1, n2, 12, True)
        >>> print(arc1.getEstInverse())
        True
        """
        pass
        return self.estInverse

# ================================================================
# =========== SETTERS ============================================
# ================================================================
    def setNom(self, nouveauNom: str) -> None:
        """Redéfinit le nom de l'Arc.

        Paramètres
        ----------
        nouveauNom : str - Le nouveau nom de l'arc

        Remarques
        ---------
        À implémenter au besoin. Deux arcs ne doivent pas avoir le même nom !

        Tests
        -----
        """
        pass
        assert isinstance(nouveauNom, str), "Le nom d'un arc doit être un string."  # noqa : E501
        self.nom = nouveauNom

    def setPoids(self, nouveauPoids: int) -> None:
        """Redéfinit le poids de l'Arc.

        Paramètres
        ----------
        nouveauPoids : int - Le nouveau poids de l'arc

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('totn2', n1, n2, 12)
        >>> arc.getPoids()
        12
        >>> arc.setPoids(8)
        >>> arc.getPoids()
        8
        """
        pass
        assert isinstance(nouveauPoids, int) or nouveauPoids is None, "Le poids d'un arc doit être un entier ou None."  # noqa: E501
        self.poids = nouveauPoids

    def setNoeudDebut(self, nouveauNoeudDebut: Noeud) -> None:
        """Redéfinit le noeud de début de l'Arc.

        Paramètres
        ----------
        nouveauNoeudDebut : Noeud - Le nouveau noeud de debut de l'arc

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> arc1 = Arc('n1n2', n1, n2, 1)
        >>> print(arc1)
        Arc(nom=n1n2, noeudDebut=Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0), noeudFin=Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0), poids=1, estInverse=False)
        >>> arc1.setNoeudDebut(n3)
        >>> print(arc1)
        Arc(nom=n1n2, noeudDebut=Noeud(nom=n3, voisins=['n2'], nbArcs=1, capital=0), noeudFin=Noeud(nom=n2, voisins=['n3'], nbArcs=1, capital=0), poids=1, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3'], nbArcs=1, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n2'], nbArcs=1, capital=0)
        >>> arc2 = Arc('n3n2', n3, n2, 2)
        >>> arc3 = Arc('n2n3', n2, n3, 3)
        >>> arc4 = Arc('n2n1', n2, n1, 4)
        >>> print(arc2)
        Arc(nom=n3n2, noeudDebut=Noeud(nom=n3, voisins=['n2'], nbArcs=3, capital=0), noeudFin=Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=4, capital=0), poids=2, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=4, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n2'], nbArcs=3, capital=0)
        >>> arc2.setNoeudDebut(n1)
        >>> print(arc2)
        Arc(nom=n3n2, noeudDebut=Noeud(nom=n1, voisins=['n2'], nbArcs=2, capital=0), noeudFin=Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=4, capital=0), poids=2, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=2, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=4, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n2'], nbArcs=2, capital=0)
        """  # noqa: E501
        pass
        assert isinstance(nouveauNoeudDebut, Noeud), "Le nouveau noeud de début d'un arc doit être une instance de la classe Noeud."  # noqa: E501

        # Si nouveauNoeudDebut est déjà voisin de self.noeudFin,
        # On incrémente uniquement le nombre d'arcs de nouveauNoeudDebut
        # Sinon on l'ajoute au voisinage
        if self.noeudFin.isVoisin(nouveauNoeudDebut):
            nouveauNoeudDebut.setNbArcs(nouveauNoeudDebut.getNbArcs() + 1)
        else:
            self.noeudFin.addVoisin(nouveauNoeudDebut)

        # Si nombre d'arcs de self.noeudDebut vaut 1, il n'aura plus de voisin
        # On peut le supprimer du voisinage de noeudFin
        # car c'est forcément son seul voisin.
        # Sinon on décrémente le nombre d'arcs de self.noeudDebut
        # ============ Remarque
        # On ne peut pas savoir s'il reste voisin de noeudFin ou pas,
        # on le gère dans la classe Graphe car ici, on ne peut pas compter
        # le nombre d'arcs parallèles entre deux noeuds.
        # ============
        if self.noeudDebut.getNbArcs() == 1:
            self.noeudFin.removeVoisin(self.noeudDebut)
        else:
            self.noeudDebut.setNbArcs(self.noeudDebut.getNbArcs() - 1)

        # On remplace alors le noeud de début de l'arc
        self.noeudDebut = nouveauNoeudDebut

    def setNoeudFin(self, nouveauNoeudFin: Noeud) -> None:
        """Redéfinit le noeud de fin de l'Arc.

        Paramètres
        ----------
        nouveauNoeudFin : Noeud - Le nouveau noeud de fin de l'arc

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> arc1 = Arc('n1n2', n1, n2, 1)
        >>> print(arc1)
        Arc(nom=n1n2, noeudDebut=Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0), noeudFin=Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0), poids=1, estInverse=False)
        >>> arc1.setNoeudFin(n3)
        >>> print(arc1)
        Arc(nom=n1n2, noeudDebut=Noeud(nom=n1, voisins=['n3'], nbArcs=1, capital=0), noeudFin=Noeud(nom=n3, voisins=['n1'], nbArcs=1, capital=0), poids=1, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n3'], nbArcs=1, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=[], nbArcs=0, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n1'], nbArcs=1, capital=0)
        >>> arc2 = Arc('n3n2', n3, n2, 2)
        >>> arc3 = Arc('n2n3', n2, n3, 3)
        >>> arc4 = Arc('n2n1', n2, n1, 4)
        >>> print(arc2)
        Arc(nom=n3n2, noeudDebut=Noeud(nom=n3, voisins=['n1', 'n2'], nbArcs=3, capital=0), noeudFin=Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=3, capital=0), poids=2, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n3', 'n2'], nbArcs=2, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=3, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n1', 'n2'], nbArcs=3, capital=0)
        >>> arc2.setNoeudFin(n1)
        >>> print(arc2)
        Arc(nom=n3n2, noeudDebut=Noeud(nom=n3, voisins=['n1', 'n2'], nbArcs=3, capital=0), noeudFin=Noeud(nom=n1, voisins=['n3', 'n2'], nbArcs=3, capital=0), poids=2, estInverse=False)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n3', 'n2'], nbArcs=3, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3', 'n1'], nbArcs=2, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n1', 'n2'], nbArcs=3, capital=0)
        """  # noqa: E501
        pass
        assert isinstance(nouveauNoeudFin, Noeud), "Le nouveau noeud de fin d'un arc doit être une instance de la classe Noeud."  # noqa: E501

        # Si nouveauNoeudFin est déjà voisin de self.noeudDebut,
        # on incrémente uniquement le nombre d'arcs de nouveauNoeudFin
        # Sinon on l'ajoute au voisinage
        if self.noeudDebut.isVoisin(nouveauNoeudFin):
            nouveauNoeudFin.setNbArcs(nouveauNoeudFin.getNbArcs() + 1)
        else:
            self.noeudDebut.addVoisin(nouveauNoeudFin)

        # Si nombre d'arcs de self.noeudFin vaut 1, il n'aura plus de voisin
        # On peut le supprimer du voisinage de noeudDebut
        # car c'est forcément son seul voisin.
        # Sinon on décrémente le nombre d'arcs de self.noeudFin
        # ============ Remarque
        # On ne peut pas savoir s'il reste voisin de noeudDebut ou pas,
        # on le gèrer dans la classe Graphe car ici, on ne peut pas compter
        # le nombre d'arcs parallèles entre deux noeuds.
        # ============
        if self.noeudFin.getNbArcs() == 1:
            self.noeudDebut.removeVoisin(self.noeudFin)
        else:
            self.noeudFin.setNbArcs(self.noeudFin.getNbArcs() - 1)

        # On remplace alors le noeud de fin de l'arc
        self.noeudFin = nouveauNoeudFin

    def setEstInverse(self, nouveauEstInverse: bool) -> None:
        """Redéfinit le booléen qui permet de marquer un arc inverse.

        Paramètres
        ----------
        nouveauEstInverse : bool - nouveau booléen permettant de définir si un arc est un arc inverse.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> arc = Arc('n1n2', n1, n2, 12, False)
        >>> print(arc.getEstInverse())
        False
        >>> arc.setEstInverse(True)
        >>> print(arc.getEstInverse())
        True
        """  # noqa : E501
        pass
        assert isinstance(nouveauEstInverse, bool), "nouveauEstInverse doit être un booléen"  # noqa : E501

        self.estInverse = nouveauEstInverse


# ================================================================
# =========== OUTILS =============================================
# ================================================================


if __name__ == "__main__":
    import doctest
    doctest.testmod()
