#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO
"""Gestion des noeuds"""


class Noeud:
    """La classe Noeud sert pour chaque sommet(vertex) du graphe.

        Attributs
        ---------
        - nom     : str  - Identification du noeud par un nom par souci de lisibilité (affichage).
        - voisins : list - Les noeuds voisins, par défaut None, stockés dans une list.
        - nbArcs  : int  - Le nombre d'arcs partant ou arrivant à une instance de la classe.
        - capital : int  - Le capital d'une entreprise à un instant donné.

    """  # noqa : E501

    def __init__(self, nom: str, voisins=None, nbArcs=0, capital=0) -> None:
        """Constructeur

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> n2 = Noeud('n2')
        >>> n2.addVoisin(n1)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        """  # noqa : E501
        pass
        assert isinstance(nom, str), "Le nom d'un noeud doit être un string."  # noqa : E501
        assert isinstance(voisins, list) or voisins is None,"Les voisins d'un noeud sont soit une liste soit None."  # noqa : E501
        assert isinstance(nbArcs, int), "Le nombre d'arcs partant du noeud doit être un entier."  # noqa : E501
        assert isinstance(capital, int), "Le capital d'une noeud noeud doit être un entier."  # noqa : E501
        self.nom = nom
        if voisins is None:
            self.voisins = []
        else:
            self.voisins = voisins
        self.nbArcs = nbArcs
        self.capital = capital

    def __str__(self) -> str:
        """Représentation en chaine de caractères d'une instance de la classe Noeud.

        Remarque
        --------
        Pour les noeuds voisins, on n'affiche que le nom des voisins dans cette représentation.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> n2 = Noeud('n2')
        >>> n2.addVoisin(n1)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        """  # noqa : E501
        voisins = []
        for voisin in self.voisins:
            voisins.append(voisin.nom)
        return "Noeud(nom={}, voisins={}, nbArcs={}, capital={})".format(
            self.nom,
            voisins,
            self.nbArcs,
            self.capital
        )

# ================================================================
# =========== GETTERS ============================================
# ================================================================
    def getNom(self) -> str:
        """Renvoie le nom du Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n1.getNom()
        'n1'
        """
        pass
        return self.nom

    def getVoisins(self) -> list:
        """Renvoie le tableau des noeuds voisins du Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> n4 = Noeud('n4')
        >>> n1.addVoisin(n2)
        >>> n1.addVoisin(n3)
        >>> n1.addVoisin(n4)
        >>> for i in range(len(n1.getVoisins())):
        ...     print(n1.getVoisins()[i])
        ...
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        Noeud(nom=n3, voisins=['n1'], nbArcs=1, capital=0)
        Noeud(nom=n4, voisins=['n1'], nbArcs=1, capital=0)
        """
        pass
        return self.voisins

    def getNbArcs(self) -> int:
        """Renvoie le nombre d'arcs partant ou arrivant sur le Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> n4 = Noeud('n4')
        >>> n1.addVoisin(n2)
        >>> n1.addVoisin(n3)
        >>> n1.addVoisin(n4)
        >>> n1.getNbArcs()
        3
        """
        pass
        return self.nbArcs

    def getCapital(self) -> int:
        """Renvoie le capital de l'instance de Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> print(n1.getCapital())
        0
        >>> n1.setCapital(10)
        >>> print(n1.getCapital())
        10
        """
        pass
        return self.capital

# ================================================================
# =========== SETTERS ============================================
# ================================================================
    def setNom(self, nouveauNom: str) -> None:
        """Redéfinit le nom du Noeud.

        Paramètres
        ----------
        nouveauNom : str - Le nouveau nom du noeud

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> n2 = Noeud('n2')
        >>> n2.addVoisin(n1)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        >>> n1.setNom('n3')
        >>> print(n1)
        Noeud(nom=n3, voisins=['n2'], nbArcs=1, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n3'], nbArcs=1, capital=0)
        """
        pass
        assert isinstance(nouveauNom, str), "Le nom d'un noeud doit être un string."  # noqa : E501
        self.nom = nouveauNom

    def setVoisins(self, nouveauxVoisins: list) -> None:
        """Redéfinit le tableau des Voisins d'un Noeud.

        Paramètres
        ----------
        nouveauxVoisins : list - Un tableau avec les noms des voisins du noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> n4 = Noeud('n4')
        >>> n1.addVoisin(n2)
        >>> n1.addVoisin(n3)
        >>> n1.addVoisin(n4)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2', 'n3', 'n4'], nbArcs=3, capital=0)
        >>> n1.setVoisins(None)
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=[], nbArcs=0, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=[], nbArcs=0, capital=0)
        >>> print(n4)
        Noeud(nom=n4, voisins=[], nbArcs=0, capital=0)
        >>> n5 = Noeud('n5')
        >>> n6 = Noeud('n6')
        >>> n1.setVoisins([n5,n6])
        >>> print(n1)
        Noeud(nom=n1, voisins=['n5', 'n6'], nbArcs=2, capital=0)
        >>> print(n5)
        Noeud(nom=n5, voisins=['n1'], nbArcs=1, capital=0)
        >>> print(n6)
        Noeud(nom=n6, voisins=['n1'], nbArcs=1, capital=0)
        """
        pass
        assert isinstance(nouveauxVoisins, list) or nouveauxVoisins is None, "Les voisins d'un noeud doivent être un tableau ou None."  # noqa : E501
        # On supprime :
        # -> l'instance du Noeud du tableau des voisins de chaque voisin
        # -> chaque voisin actuel du tableau des voisins de l'instance du Noeud
        k = self.lengthOfVoisins()-1
        while k >= 0:
            self.removeVoisin(self.voisins[k])
            k -= 1
        # On réaffecte les nouveauxVoisins à l'instance du Noeud
        if (nouveauxVoisins is None):
            self.voisins = []
        else:
            for voisin in nouveauxVoisins:
                self.addVoisin(voisin)

    def setNbArcs(self, nouveauNbArcs: int) -> None:
        """Redéfinit le nombre d'arcs partant ou arrivant du Noeud.

        Paramètres
        ----------
        nouveauNbArcs : int - Nouveau nombre d'arcs partant/arrivant du noeud

        Remarques
        ---------
        - Cette méthode ne semble pas avoir tellement d'intérêt étant donné
        qu'on peut très bien incrémenter directement l'attribut nbArcs.
        - Peut-être pour les arcs parallèles

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n1.addVoisin(n2)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        >>> n1.setNbArcs(5)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=5, capital=0)
        """
        pass
        assert isinstance(nouveauNbArcs, int), "Le nouveau nombre d'arcs doit être un entier"  # noqa : E501
        self.nbArcs = nouveauNbArcs

    def setCapital(self, nouveauCapital: int) -> None:
        """Redéfinit le capital de l'instance de Noeud.

        Paramètres
        ----------
        nouveauCapital : int - Le nouveau capital

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> print(n1.getCapital())
        0
        >>> n1.setCapital(10)
        >>> print(n1.getCapital())
        10
        """
        pass
        assert isinstance(nouveauCapital, int), "nouveauCaptial doit être un entier."  # noqa : E501
        self.capital = nouveauCapital

# ================================================================
# =========== OUTILS =============================================
# ================================================================
    def addVoisin(self, nouveauVoisin) -> None:
        """Ajoute le noeud nouveauVoisin dans le tableau des voisins d'une instance d'un objet Noeud.

        Ajoute aussi l'instance de Noeud au tableau des voisins de nouveauVoisin !

        Incrémente le nombre d'arcs de chaque instance.

        Paramètres
        ----------
        nouveauVoisin: une instance de la classe Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=[], nbArcs=0, capital=0)
        >>> n1.addVoisin(n2)
        >>> for i in range(len(n1.getVoisins())):
        ...     print(n1.getVoisins()[i])
        ...
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        >>> for i in range(len(n2.getVoisins())):
        ...     print(n2.getVoisins()[i])
        ...
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        """  # noqa : E501
        pass
        assert isinstance(nouveauVoisin, Noeud), "nouveauVoisin doit être une instance de la classe Noeud."  # noqa : E501
        assert nouveauVoisin not in self.voisins, "On ne peut pas ajouter un voisin déjà existant."  # noqa : E501

        # Ne pas utiliser les assertions pour valider les données
        # car elles peuvent être désactivées.
        # On lève donc une erreur en cas de souci
        if nouveauVoisin in self.voisins:
            raise ValueError(f'On ne peut pas ajouter un voisin déjà existant à {self.nom}')  # noqa : E501
        else:
            # On modifie la liste des voisins de chaque noeud.
            self.voisins.append(nouveauVoisin)
            nouveauVoisin.voisins.append(self)
            # On incrémente le nombre d'arcs de chaque noeud
            self.setNbArcs(self.nbArcs + 1)
            nouveauVoisin.setNbArcs(nouveauVoisin.nbArcs + 1)

    def removeVoisin(self, ancienVoisin) -> None:
        """Supprime l'ancienVoisin du tableau des voisins d'une instance d'un objet Noeud.
        
        Supprime aussi l'instance de Noeud au tableau des voisins d'ancienVoisin !

        Paramètres
        ----------
        ancienVoisin: une instance de la classe Noeud.

        Remarques
        ---------
        Si on supprime le noeud ancienVoisin, c'est qu'il n'y a plus d'arcs qui le lie à l'instance de la classe.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> print(n1)
        Noeud(nom=n1, voisins=[], nbArcs=0, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=[], nbArcs=0, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=[], nbArcs=0, capital=0)
        >>> n1.addVoisin(n2)       
        >>> n1.addVoisin(n3)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2', 'n3'], nbArcs=2, capital=0)
        >>> print(n2)
        Noeud(nom=n2, voisins=['n1'], nbArcs=1, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=['n1'], nbArcs=1, capital=0)
        >>> n3.removeVoisin(n1)
        >>> print(n1)
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        >>> print(n3)
        Noeud(nom=n3, voisins=[], nbArcs=0, capital=0)
        """  # noqa : E501
        pass
        assert isinstance(ancienVoisin, Noeud), "ancienVoisin doit être une instance de la classe Noeud."  # noqa : E501
        assert ancienVoisin in self.voisins, "On ne peut pas supprimer un voisin qui n'existe pas."  # noqa : E501

        # Ne pas utiliser les assertions pour valider les données
        # car elles peuvent être désactivées.
        # On lève donc une erreur en cas de souci
        if ancienVoisin not in self.voisins:
            raise ValueError(f'On ne peut pas supprimer un voisin de {self.nom} qui n\'existe pas')  # noqa : E501
        else:
            # list.remove() ne supprime que le premier élément trouvé mais
            # la structure de Noeud fait qu'il n'y a pas de doublon de voisins
            self.voisins.remove(ancienVoisin)
            ancienVoisin.voisins.remove(self)
            # On décrémente le nombre d'arcs de chaque noeud
            self.setNbArcs(self.nbArcs - 1)
            ancienVoisin.setNbArcs(ancienVoisin.nbArcs - 1)

    def isVoisin(self, voisinCandidat) -> bool:
        """Renvoie un booléen selon que l'instance du Noeud est voisin de Voisin ou non.

        Paramètres
        ----------
        voisinCandidat: une instance de la classe Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n2 = Noeud('n2')
        >>> n3 = Noeud('n3')
        >>> n1.addVoisin(n2)       
        >>> n2.addVoisin(n3)
        >>> for i in range(len(n1.getVoisins())):
        ...     print(n1.getVoisins()[i])
        ...
        Noeud(nom=n2, voisins=['n1', 'n3'], nbArcs=2, capital=0)
        >>> for i in range(len(n2.getVoisins())):
        ...     print(n2.getVoisins()[i])
        ...
        Noeud(nom=n1, voisins=['n2'], nbArcs=1, capital=0)
        Noeud(nom=n3, voisins=['n2'], nbArcs=1, capital=0)
        >>> for i in range(len(n3.getVoisins())):
        ...     print(n3.getVoisins()[i])
        ...
        Noeud(nom=n2, voisins=['n1', 'n3'], nbArcs=2, capital=0)
        >>> n1.isVoisin(n2)
        True
        >>> n1.isVoisin(n3)
        False
        >>> n3.isVoisin(n2)
        True
        """  # noqa : E501
        pass
        assert isinstance(voisinCandidat, Noeud), "voisin doit être une instance de la classe Noeud."  # noqa : E501
        isVoisin = False
        if voisinCandidat in self.voisins:
            isVoisin = True

        return isVoisin

    def lengthOfVoisins(self) -> int:
        """Renvoie le nombre de voisins d'une instance de la class Noeud.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n1.lengthOfVoisins()
        0
        >>> n2 = Noeud('n2')
        >>> n1.addVoisin(n2)
        >>> n1.lengthOfVoisins()
        1
        >>> n3 = Noeud('n3')
        >>> n2.addVoisin(n3)
        >>> n2.lengthOfVoisins()
        2
        """
        pass
        return len(self.voisins)

    def hasVoisins(self) -> bool:
        """Renvoie True si le noeud a au moins un voisin connecté.

        Tests
        -----
        >>> n1 = Noeud('n1')
        >>> n1.hasVoisins()
        False
        >>> n2 = Noeud('n2')
        >>> n1.addVoisin(n2)
        >>> n1.hasVoisins()
        True
        """
        pass
        hasVoisins = False
        if (self.lengthOfVoisins() == 0):
            hasVoisins = False
        else:
            hasVoisins = True
        return hasVoisins


if __name__ == "__main__":
    import doctest
    doctest.testmod()
