#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO
"""
# MODELS - IMPLÉMENTE LE TYPE GRAPHE ET SES INSTANCES À MANIPULER

Sous-module permettant de stocker les **objets** manipulant les bases de données.
Le modèle représente les données qui vont être utilisées dans l’application.
C’est ici que vont être stockées les données, et tout ce qui permet de la modifier (getters, setters, etc.).

Ce sont littéralement des modèles de données.

C'est ici que nous allons implémenter la structure de graphe.

- un noeud est identifié par un label de type chaine de caractères
- le graphe est orienté
- le graphe est pondéré
- il peut y avoir entre deux noeuds plusieurs arêtes de même poids
- les valeurs des noeuds et des arêtes sont non typés, le type vient avec l'usage
- Pour créer un graphe non pondéré, ou des noeuds sans valeur, on utilise la valeur None

Le problème peut être modéliser à l'aide cette structure :

- Chaque entreprise est un sommet/noeud, par souci de lisibilité (affichage), une entreprise pourra être identifiée par un nom.
- Une facture due d’une entreprise A vers une entreprise B est modélisée par un arc étiqueté du sommet A vers le sommet B.
- Il peut y avoir plusieurs arcs d’un nœud vers un autre.
- Il peut y avoir des arcs du nœud A vers le nœud B, et aussi du nœud B vers le nœud A.
"""  # noqa : E501

# Pour gérer les imports relatifs
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
