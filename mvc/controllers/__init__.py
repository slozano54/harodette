#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO
"""
# CONTROLLERS - GÈRE L'INTERACTION ENTRE LE MODÈLE ET LA VUE

Sous-module qui gère l’interaction entre la vue et le modèle.
Lorsque une interaction utilisateur est faite, le contrôleur va dire au modèle
de changer ses données. Et lorsque les données changent, il va recevoir un
événement du modèle et envoyer un événement à la vue.

Le contrôleur, ou controller, est l’élément qui contient la logique métier.
Ce sont la plupart des algorithmes, calculs, etc.

C’est aussi l’intermédiaire principal entre la vue et le modèle. Par exemple,
la vue soumet un formulaire au contrôleur, qui gère sa validation via du code
métier, et demande au modèle de faire des modifications
dans la base de données.

Dans le cadre de ce projet, il n'y a pas de base de données, les objets
sont modifiés au fur et à mesure.
"""  # noqa : E501

# Pour gérer les imports relatifs
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
