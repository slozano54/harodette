#!/usr/bin/python3
# -*- coding: utf8 -*-

# @author : Sébastien LOZANO
"""
# VIEWS - GÈRE LES INTERACTIONS AVEC L'UTILISATEUR

C'est la partie qui permet d'interagir avec l'utilisateur.
Affichages et entrées utilisateur.

La vue représente les données contenues dans le modèle.

La vue, ou view, c’est l’interface "graphique" de l’application.
C’est via cet élément que vont se faire les interactions
entre l’utilisateur et le code métier.
Elle ne contient presque aucune logique, son but est de construire,
à partir de ce que renvoie le serveur, une interface et de l’afficher
à l’utilisateur.

Dans le cadre du projet, il n'y a pas de serveur à proprement parler
mais des fichiers sont générés dans l'arborescence.
"""

# Pour gérer les imports relatifs
import os
import sys
sys.path.append(os.path.dirname(os.path.realpath(__file__)))
