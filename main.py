#!/usr/bin/python3
# *- coding: utf8 -*-

# @author : Sébastien LOZANO

"""Programme principal"""
pass

# Pour mesurer le temps de traitement du script
from datetime import datetime  # noqa : E402

# Pour les copies
import copy  # noqa : E402

# Pour la gestion des fichiers
import sys   # noqa : E402
import os  # noqa : E402

# Pour la gestion d'un navigateur
import webbrowser  # noqa : E402

from mvc.models import model  # noqa : E402

from mvc.controllers import controller  # noqa : E402

from mvc.views import view  # noqa : E402
from mvc.views.view import GRAPHE_PSEUDO_REELS # noqa : E402


def makeIndexHtml() -> None:
    """Crée une page html d'index avec autant de liens que de graphes
    traités dans les dossiers ./vuesTexteEtHtml/graphviz_*

    Copie aussi les fichiers javascript nécessaires à l'affichage des graphes
    """
    pass

    #  Page vide
    header = """<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<title>GraphViz</title>
</head>
<body>\n"""
    footer = """</body>\n</html>"""
    # On crée le répertoire s'il n'existe pas
    htmlDir = './vuesTexteEtHtml'
    jsDir = htmlDir + '/js'
    indexHtml = htmlDir + '/index.html'
    eliminationCycles = {
        'dir': './graphviz_eliminationCycles',
        'h2': 'Méthode par élimination des Cycles'
    }
    heuristique = {
        'dir': './graphviz_heuristique',
        'h2': 'Méthode heuristique'
    }
    klein = {
        'dir': './graphviz_klein',
        'h2': 'Algorithme de Klein'
    }
    dirsToScan = [heuristique, eliminationCycles, klein]
    # Normalement le répertoire existe mais on ne sait jamais
    if not os.path.exists(htmlDir):
        os.makedirs(htmlDir)
    if not os.path.exists(jsDir):
        os.makedirs(jsDir)
    # On copie les fichiers js utile pour l'affichage des graphes
    my_os = sys.platform
    fileToCopy1 = './assets/viz.js'
    fileToCopy2 = './assets/full.render.js'
    destFile1 = htmlDir + '/js/viz.js'
    destFile2 = htmlDir + '/js/full.render.js'
    command = ''
    if my_os in ['linux', 'darwin']:
        command = 'cp'
    elif my_os == 'windows':
        command = 'copy'
    else:
        print('os non reconnu !')
    os.system(command + " " + fileToCopy1 + " " + destFile1)
    os.system(command + " " + fileToCopy2 + " " + destFile2)
    with open(indexHtml, 'w') as file:
        file.write(header)
        file.write('<h1>Réductions des graphes</h1>\n')
        file.write("""<p>Vers les pages contenant les graphes obtenus par application des algorithmes sur quelques graphes trouvés dans le cours ou les sources du sujet.</p>\n""") # noqa : E501
        file.write("""<p>Quelques essais sur le graphe Gen100 d'Arthur Rousseau sauf pour l'algorithme de Klein.</p>\n""") # noqa : E501
        for dir in dirsToScan:
            file.write('<h2>'+dir["h2"]+'</h2>\n')
            file.write('<ul>\n')
            for subDir in os.listdir(htmlDir + '/' + dir["dir"]):
                for path in os.listdir(htmlDir + '/' + dir["dir"]+'/'+subDir):
                    if os.path.isfile(os.path.join(htmlDir + '/' + dir["dir"]+'/'+subDir, path)):  # noqa : E501
                        currentDotFile = open(os.path.join(htmlDir + '/' + dir["dir"]+'/'+subDir, path), "r+")  # noqa : E501
                        root, extension = os.path.splitext(path)
                        if extension == '.html':
                            if root == 'gch':
                                file.write("<li><a href='"+dir["dir"]+"/"+subDir+"/"+path+"' target='_self'>Graphe du cours heuristique</a></li>\n") # noqa : E501
                            elif root == 'grousseau':
                                file.write("<li><a href='"+dir["dir"]+"/"+subDir+"/"+path+"' target='_self'>Graphe du mémoire biblio d'Arthur Rousseau</a></li>\n") # noqa : E501
                            elif root == 'gmikhailiv':
                                file.write("<li><a href='"+dir["dir"]+"/"+subDir+"/"+path+"' target='_self'>Graphe du rapport de Yosip Mikhailiv</a></li>\n") # noqa : E501
                            elif root == 'gvelamena':
                                file.write("<li><a href='"+dir["dir"]+"/"+subDir+"/"+path+"' target='_self'>Graphe du rapport de Marie Vela-Mena</a></li>\n") # noqa : E501                        
                            elif root in GRAPHE_PSEUDO_REELS:
                                if root == GRAPHE_PSEUDO_REELS[0]:
                                    file.write("<h3>FACULTATIF</h3>")
                                    file.write("""<p> Gen100 correspondant au graphe Gen_100 généré par Arthur Rousseau, Gen11Cycles à un graphe pour tester le parser</p>""") # noqa : E501
                                    file.write("<ul>")
                                file.write("<li><a href='"+dir["dir"]+"/"+subDir+"/"+path+"' target='_self'>Graphe "+root[1:].capitalize()+"</a></li>\n") # noqa : E501                        
                                if root == GRAPHE_PSEUDO_REELS[len(GRAPHE_PSEUDO_REELS)-1]: # noqa : E501
                                    file.write("</ul>")
                        currentDotFile.close()
            file.write('</ul>\n')

        file.write(footer)
        file.close()


def makeAll(situation: controller.Controleur, baseName: str) -> None:
    """Construit les fichiers *.dot,, *.html, *.txt à partir du controller.
    """
    pass
    assert isinstance(situation, controller.Controleur), "situation doit être un Controleur" # noqa : E501

    # On lance l'algorithme ad hoc
    if situation.getVue().getAlgo() == 'heuristique':
        situation.lancerMethodeHeuristique()
        # situation.getVue().consoleHeuristique()
    elif situation.getVue().getAlgo() == 'eliminationCycles':
        situation.lancerReductionParEliminationDesCycles()
        # situation.getVue().consoleEliminationCycles()
    elif situation.getVue().getAlgo() == 'klein':
        situation.lancerAlgorithmeDeKlein()
        # situation.getVue().consoleAlgorithmeDeKlein()

    # On crée les fichiers *.dot
    situation.getVue().graphvizMakeDotAndJsFiles(baseName)
    # On crée la sortie texte
    situation.getVue().texteMakeFile(baseName)
    # On crée le fichier html
    situation.getVue().graphvizMakeHtml(baseName)


def main(choice, startTime):

    # ===============================================================
    # =========== GRAPHES TEST ======================================
    # ===============================================================

    # =========== ALGORITHME 1 : METHODE HEURISTIQUE ================
    gch1 = copy.deepcopy(model.grapheCoursHeuristique)
    vue_gch1 = view.Vue(gch1, None, "heuristique")
    situation_gch1 = controller.Controleur(gch1, vue_gch1)

    gr1 = copy.deepcopy(model.grapheRousseau)
    vue_gr1 = view.Vue(gr1, None, "heuristique")
    situation_gr1 = controller.Controleur(gr1, vue_gr1)

    gm1 = copy.deepcopy(model.grapheMikhailiv)
    vue_gm1 = view.Vue(gm1, None, "heuristique")
    situation_gm1 = controller.Controleur(gm1, vue_gm1)

    gv1 = copy.deepcopy(model.grapheVelaMena)
    vue_gv1 = view.Vue(gv1, None, "heuristique")
    situation_gv1 = controller.Controleur(gv1, vue_gv1)

    # =========== ALGORITHME 2 : ÉLIMINATION DES CYCLES =============
    gch2 = copy.deepcopy(model.grapheCoursHeuristique)
    vue_gch2 = view.Vue(gch2, None, "eliminationCycles")
    situation_gch2 = controller.Controleur(gch2, vue_gch2)

    gr2 = copy.deepcopy(model.grapheRousseau)
    vue_gr2 = view.Vue(gr2, None, "eliminationCycles")
    situation_gr2 = controller.Controleur(gr2, vue_gr2)

    gm2 = copy.deepcopy(model.grapheMikhailiv)
    vue_gm2 = view.Vue(gm2, None, "eliminationCycles")
    situation_gm2 = controller.Controleur(gm2, vue_gm2)

    gv2 = copy.deepcopy(model.grapheVelaMena)
    vue_gv2 = view.Vue(gv2, None, "eliminationCycles")
    situation_gv2 = controller.Controleur(gv2, vue_gv2)

    # =========== ALGORITHME 3 : ALGORITHME DE KLEIN ================
    gch3 = copy.deepcopy(model.grapheCoursHeuristique)
    vue_gch3 = view.Vue(gch3, None, "klein")
    situation_gch3 = controller.Controleur(gch3, vue_gch3)

    gr3 = copy.deepcopy(model.grapheRousseau)
    vue_gr3 = view.Vue(gr3, None, "klein")
    situation_gr3 = controller.Controleur(gr3, vue_gr3)

    gm3 = copy.deepcopy(model.grapheMikhailiv)
    vue_gm3 = view.Vue(gm3, None, "klein")
    situation_gm3 = controller.Controleur(gm3, vue_gm3)

    gv3 = copy.deepcopy(model.grapheVelaMena)
    vue_gv3 = view.Vue(gv3, None, "klein")
    situation_gv3 = controller.Controleur(gv3, vue_gv3)

    # ===============================================================
    # =========== GRAPHES DE TAILLE RÉELLE ==========================
    # ===============================================================

    # =========== LES TESTS SUR CES GRAPHES SONT FACULTATIFS ========
    # =========== TESTS MISE EN PLACE DU PARSER =====================
    gen11Cycles_1 = copy.deepcopy(model.grapheGen11Cycles)
    vue_gen11Cycles_1 = view.Vue(gen11Cycles_1, None, "heuristique")
    situation_gen11Cycles_1 = controller.Controleur(
        gen11Cycles_1,
        vue_gen11Cycles_1
    )

    gen11Cycles_2 = copy.deepcopy(model.grapheGen11Cycles)
    vue_gen11Cycles_2 = view.Vue(gen11Cycles_2, None, "eliminationCycles")
    situation_gen11Cycles_2 = controller.Controleur(
        gen11Cycles_2,
        vue_gen11Cycles_2
    )

    gen11Cycles_3 = copy.deepcopy(model.grapheGen11Cycles)
    vue_gen11Cycles_3 = view.Vue(gen11Cycles_3, None, "klein")
    situation_gen11Cycles_3 = controller.Controleur(
        gen11Cycles_3,
        vue_gen11Cycles_3
    )

    # =========== ALGORITHME 1 : METHODE HEURISTIQUE ================
    gen100_1 = copy.deepcopy(model.grapheGen100)
    vue_gen100_1 = view.Vue(gen100_1, None, "heuristique")
    situation_gen100_1 = controller.Controleur(gen100_1, vue_gen100_1)

    # =========== ALGORITHME 2 : ÉLIMINATION DES CYCLES =============
    gen100_2 = copy.deepcopy(model.grapheGen100)
    vue_gen100_2 = view.Vue(gen100_2, None, "eliminationCycles")
    situation_gen100_2 = controller.Controleur(gen100_2, vue_gen100_2)

    # =========== ALGORITHME 3 : ALGORITHME DE KLEIN ================
    gen100_3 = copy.deepcopy(model.grapheGen100)
    vue_gen100_3 = view.Vue(gen100_3, None, "klein")
    situation_gen100_3 = controller.Controleur(gen100_3, vue_gen100_3)

    # ===============================================================
    # =========== MENU DE LANCEMENT DU PROG PRINCIPAL ===============
    # ===============================================================

    my_os = sys.platform
    clearCommand = ''
    if my_os in ['linux', 'darwin']:
        clearCommand = 'clear'
    elif my_os == 'windows':
        clearCommand = 'cls'
    else:
        print('os non reconnu !')    # On choisit ce qu'on fait
    while choice not in ['1', '2', '3', '4', '5']:
        choice = input("""Que fait-il lancer ?
        ---> 1 : Algos sur les 4 graphes
        ---> 2 : Algos sur parserTest
        ---> 3 : Algos sur Gen100
        ---> 4 : Algos sur tout
        ---> 5 : Quitter
Taper 1, 2, 3 ou 4 pour lancer le script --> """)

        if choice in ['1', '4']:  # 4 Graphes ou tout
            makeAll(situation_gch1, 'gch')
            makeAll(situation_gr1, 'grousseau')
            makeAll(situation_gm1, 'gmikhailiv')
            makeAll(situation_gv1, 'gvelamena')
            makeAll(situation_gch2, 'gch')
            makeAll(situation_gr2, 'grousseau')
            makeAll(situation_gm2, 'gmikhailiv')
            makeAll(situation_gv2, 'gvelamena')
            makeAll(situation_gch3, 'gch')
            makeAll(situation_gr3, 'grousseau')
            makeAll(situation_gm3, 'gmikhailiv')
            makeAll(situation_gv3, 'gvelamena')

        # =========== Remarque
        # Les strings zgen22Cycles, zgen100, ... permettent de gérer
        # l'ordre d'affichage de la sortie HTML

        elif choice in ['2', '4']:  # parserTest ou tout
            makeAll(situation_gen11Cycles_1, 'zgen011Cycles')
            makeAll(situation_gen11Cycles_2, 'zgen011Cycles')
            # Pour éviter l'erreur
            # NameError: name 'allArcsKleinCyclesCleaned' is not defined
            # allArcsKleinCyclesCleaned est une variable globale
            # nécessaire dans la méthode kleinAllArcsCyclesFromNoeud()
            # de la classe Graphe()
            global allArcsKleinCyclesCleaned
            makeAll(situation_gen11Cycles_3, 'zgen011Cycles')

        elif choice in ['3', '4']:  # gen100 ou tout
            makeAll(situation_gen100_1, 'zgen100')
            makeAll(situation_gen100_2, 'zgen100')
            # makeAll(situation_gen100_3, 'zgen100')
        elif choice == '5':
            sys.exit(0)
        else:
            os.system(clearCommand)
            print('/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\')
            print('--- CHOIX INCORRECT ---')
            print('/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\')

    # ===============================================================
    # =========== RELANCE OU CRÉATION ET LANCEMENT DE L'INDEX HTML ==
    # ===============================================================

    restart = ''
    while restart not in ['o', 'n']:
        # On évalue le temps de traitement
        endTime = datetime.now()
        print("==========================================================")
        print("  Durée intermédiaire de traitement : ", endTime-startTime)
        print("==========================================================")
        print("***********************************************************")
        print("******* Relancer les algos ?")
        print(" ")
        restart = input("******* Taper o ou n pour lancer le script --> ")
        if restart == 'o':
            choice = 0
            main(choice, startTime)
        elif restart == 'n':
            # On évalue le temps de traitement
            endTime = datetime.now()
            print("==========================================================")
            print("  Durée totale de traitement : ", endTime-startTime)
            print("==========================================================")
            # On crée l'index html principal
            makeIndexHtml()
            # On lance le fichier html principal
            webbrowser.open_new_tab('vuesTexteEtHtml/index.html')
        else:
            os.system(clearCommand)
            print('/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\')
            print('--- CHOIX INCORRECT ---')
            print('/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\/!\\')


if __name__ == "__main__":
    # On récupère la date au début du traitement
    startTime = datetime.now()
    print("===========================================================")
    print("  Début du traitement : ", startTime-startTime)
    print("===========================================================")
    choice = 0
    main(choice, startTime)
